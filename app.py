import dash
import dash_core_components as dcc
import dash_html_components as html
from rq import Queue, cancel_job
from rq.job import Job
from redis import Redis
from tasks import main_function
from utilities import is_url, human_delta
from datetime import datetime as dt
from datetime import timedelta as td
from flask import Flask, send_from_directory, session, request
import pandas as pd
import os
import json
import plotly.graph_objs as go

REDIS_HOST = os.environ.get('REDIS_SERVICE_HOST', 'redis')
REDIS_PORT = os.environ.get('REDIS_SERVICE_PORT', '6379')
FLASK_DEBUG = True if os.environ.get('FLASK_DEBUG') else False
FLASKHOST = os.environ.get('FLASKHOST', '0.0.0.0')
FLASKPORT = int(os.environ.get('FLASKPORT', 5000))
RESULT_TTL = int(os.environ.get('RESULT_TTL', 3600))
DATA_DIR = '/data'

redis_conn = Redis(host=REDIS_HOST, port=int(REDIS_PORT))
print('Pinging redis ', redis_conn.ping())
q = Queue('default', connection=redis_conn)  # no args implies the default queue
external_stylesheets = ['//cdnjs.cloudflare.com/ajax/libs/milligram/1.3.0/milligram.css']
server = Flask(__name__)
server.secret_key = os.environ.get('SECRET_KEY', '131oin3ofi2noifn')
app = dash.Dash(server=server, external_stylesheets=external_stylesheets)
app.config.suppress_callback_exceptions = True


@server.route('/download/<path:path>')
def download(path):
    return send_from_directory(DATA_DIR, path, as_attachment=True)

# ---- MAIN LAYOUT -----
# It contains basically nothing. We use the 
# function display_page() to decide which layout apply based
# on the route
# #page-content is used to as an output to inject the content
# from display_page
# url is used as an input to parse the url and feed it to display_page()
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content'),
])

# This should be the page on path /
# It has the search form to submit the queries
index_page = html.Div([
    html.H1('Timber data and plot retrieval'),
    html.Div([
        html.Div('This website allows you to ...')
    ], className='row'),
    html.Form([
        html.Fieldset([
            html.Label('Query to perform', htmlFor='query-field'),
            dcc.Input(id='query-field', value='', type='text', placeholder='Ex: ATLRPC_DI_PT61%.POSST'),
            html.Label('Date range', htmlFor='datetime-range'),
            dcc.DatePickerRange(
                id='datetime-range',
                min_date_allowed=dt(2017, 1, 1),
                start_date=dt.now() - td(days=10),
                end_date=dt.now(),
                max_date_allowed=dt.now(),
            ),
            dcc.Checklist(id='checklist-send-email', options=[{
                'label': 'Send Email when task is done', 'value': 'send-email'
            }]),
            html.Button('Send my task', id='submit-task-button', n_clicks=0, type='button', className='button-primary'),
        ])
    ]),
    html.Div([
        html.Br(),
        html.Div(f'Info: retrieved data is kept for {RESULT_TTL} seconds.')
    ]),
    html.Div(id='jobs-submit-info', style={'display': None}),
    html.Div(id='enqueued-jobs-list'),
    html.Div(id='finished-jobs-list'),
    dcc.Interval(
        id='interval-component',
        interval=1*2000, # in milliseconds
        n_intervals=0
    )
], className='container')


# This is the app that is updating the index page every second with the status
# of the jobs
@app.callback(dash.dependencies.Output('finished-jobs-list', 'children'),
              [dash.dependencies.Input('interval-component', 'n_intervals')])
def update_jobs_section(n):
    running_jobs = [Job.fetch(job_id, connection=redis_conn) for job_id in q.started_job_registry.get_job_ids()]
    finished_jobs = list(reversed([Job.fetch(job_id, connection=redis_conn) for job_id in q.finished_job_registry.get_job_ids()]))
    content_running_jobs = 'No running jobs'
    content_finished_jobs = 'No jobs here'

    headers_running = ['Id', 'Started at', 'User', 'Query', 'Status', 'Abort']
    headers_finished = ['Id', 'Started at', 'User', 'Query', 'Status']

    thead_running =  html.Thead(html.Tr([html.Th(item) for item in headers_running]))
    thead_finished = html.Thead(html.Tr([html.Th(item) for item in headers_finished]))

    tbody_running = html.Tbody([html.Tr([
        html.Td(dcc.Link(href=f'/{job.get_id()}')),
        html.Td(job.enqueued_at),
        html.Td(job.meta.get('email', 'N/A')),
        html.Td(html.I(job.meta.get('query_str'))),
        html.Td(job.get_status()),
        html.Td(html.Button('Abort', id={'type': 'abort', 'index': job.get_id()}, value=job.get_id(), type='submit', name='abort', className='button-primary'))
    ]) for job in running_jobs ])

    tbody_finished = html.Tbody([html.Tr([
        html.Td(dcc.Link(href=f'/{job.get_id()}')),
        html.Td(job.enqueued_at),
        html.Td(job.meta.get('email', 'N/A')),
        html.Td(html.I(job.meta.get('query_str'))),
        html.Td(job.get_status()),
    ]) for job in finished_jobs ])

    table_running = html.Table([thead_running, tbody_running])
    table_finished = html.Table([thead_finished, tbody_finished])

    if len(running_jobs):
        content_running_jobs = table_running
    if len(finished_jobs):
        content_finished_jobs = table_finished
    return html.Div([
        html.H3('Running jobs:'),
        html.Div(content_running_jobs),
        html.H3('Finished jobs:'),
        html.Div(content_finished_jobs),
    ])

@app.callback(dash.dependencies.Output('jobs-submit-info', 'value'),
              [dash.dependencies.Input({'type': 'abort', 'index': dash.dependencies.ALL}, 'value')])
def abort_job(values):
    for value in values:
        cancel_job(value, connection=redis_conn)


# this is a function returning the job from its id with its content
def show_job_page(job_id):
    job = Job.fetch(job_id, connection=redis_conn)
    status = job.get_status()
    result = job.result
    enqueued_at = job.enqueued_at
    ended_at = job.ended_at
    query_str = job.meta.get('query_str')
    points = job.meta.get('points', 0)
    method = job.meta.get('method', 'aligned')
    duration = human_delta(td(seconds=job.meta.get('duration', 0))) 
    # Create plot object
    fig = go.Figure(layout=go.Layout())
    for key, (time, value) in result.items():
        fig.add_trace(go.Scatter(x=time, y=value, mode='markers+lines', name=key, line_shape='hv'))
    fig['layout']['height'] = 700
    graph = dcc.Graph(
        id='plot',
        figure=fig,

        config={
            'scrollZoom': True,
            'editable': True,
            'toImageButtonOptions': {
                'format': 'png',
                'filename': f'{job_id}',
                'height': 700,
                'width': 900
            }
        }

    )

    return html.Div([
        html.Div([
            html.Table([
                html.Thead(html.Tr([
                    html.Th('Id'),
                    html.Th('Status'),
                    html.Th('Query'),
                    html.Th('Points retrieved'),
                    html.Th('Enqueued at'),
                    html.Th('Ended at'),
                    html.Th('Duration'),
                    html.Th('Download'),
                ])),
                html.Tbody(html.Tr([
                    html.Td(html.I(job_id)),
                    html.Td(status),
                    html.Td(html.I(query_str)),
                    html.Td(points),
                    html.Td(enqueued_at.strftime("%Y-%m-%d %H:%M")),
                    html.Td(ended_at.strftime("%Y-%m-%d %H:%M")),
                    html.Td(duration),
                    html.Td(html.A(f'{method} csv', href=f'/download/{job_id}-{method}.csv')),
                ]))
            ]),
        ], className="row"),
        html.Div([
            dcc.Loading(id='graph-loading', children=[html.Div(graph)], type="default")
        ])
    ], className="container")


    return html.Div([
        html.Div([
            html.Div([html.H5('Status'), html.Pre(status)], className='four columns'),
            html.Div([html.H5('id'), html.Pre(job_id)], className='four columns'),
            html.Div([html.H5('Query'), html.Pre(query_str)], className='four columns'),
            html.Div([html.H5('Points'), html.Pre(points)], className='four columns'),
        ], className='row'),
        html.Div([
            html.Div([html.H5('Query duration'), html.Pre(duration)], className='four columns'),
            html.Div([html.H5('enqueued_at'), html.Pre(enqueued_at)], className='four columns'),
            html.Div([html.H5('ended_at'), html.Pre(ended_at)], className='four columns'),
            html.Div([html.H5('Download'), html.A('.csv', href=f'/download/{job_id}-{method}.csv')], className='four columns'),
        ], className='row'),
        html.Div([html.H5('result'), graph], className='row')
    ])


@app.callback(dash.dependencies.Output('plot', 'fig'),
             [dash.dependencies.Input('plot', 'value')])
def graph_loading_state(value):
    return value


def page_not_found():
    return 404, 'Page not found'


@app.callback(dash.dependencies.Output('jobs-submit-info', 'children'),
             [dash.dependencies.Input('submit-task-button', 'n_clicks')],
             [dash.dependencies.State('query-field', 'value'),
             dash.dependencies.State('datetime-range', 'start_date'),
             dash.dependencies.State('datetime-range', 'end_date'),
             dash.dependencies.State('checklist-send-email', 'value')])
def submit_job(n_clicks, value, start_date, end_date, checklist_send_email):
    if n_clicks == 0: return
    email = session.get('X-Remote-Email', 'N/A')
    if checklist_send_email and 'send-email' in checklist_send_email:
        email_to = email
    else:
        email_to = None
    if start_date == None or end_date == None:
        return html.Div('Please provide fill in both start and end dates')
    start_date = pd.to_datetime(start_date)
    end_date = pd.to_datetime(end_date)
    print(f'About to submit a job {value} with email', flush=True)
    # job = q.enqueue(count_words_at_url, value)
    job = q.enqueue(main_function, value, start_date, end_date, email_to, 
                    result_ttl=RESULT_TTL)
    query_str = f'SELECT {value} FROM {start_date.strftime("%Y-%m-%d %H:%M")} TO {end_date .strftime("%Y-%m-%d %H:%M")}'
    job.meta['query_str'] = query_str
    job.meta['email'] = email
    job.save_meta()
    return html.Div([
            html.H3('Your job id is: '),
            dcc.Link(job.id, href=f'/{job.id}')
    ])


@app.callback(dash.dependencies.Output('page-content', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def display_page(pathname):
    if not pathname or pathname == '/':
        credentials_list = ['X-Remote-Firstname', 'X-Remote-Lastname', 'X-Remote-User', 'X-Remote-Email']
        for item in credentials_list:
            session[item] = request.headers.get(item, 'N/A')
        return index_page
    else:
        job_id = pathname[1:]
        if Job.exists(job_id, connection=redis_conn):
            return show_job_page(job_id)
    return page_not_found()

if __name__ == '__main__':
    app.run_server(debug=True, port=FLASKPORT, host=FLASKHOST)