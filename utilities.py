from urllib.parse import urlparse
from pathlib import Path
import pandas as pd

def is_url(url):
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False

def human_delta(tdelta):
    """
    Takes a timedelta object and formats it for humans.
    Usage:
        # 149 day(s) 8 hr(s) 36 min 19 sec
        print human_delta(datetime(2014, 3, 30) - datetime.now())
    Example Results:
        23 sec
        12 min 45 sec
        1 hr(s) 11 min 2 sec
        3 day(s) 13 hr(s) 56 min 34 sec
    :param tdelta: The timedelta object.
    :return: The human formatted timedelta
    """
    d = dict(days=tdelta.days)
    d['hrs'], rem = divmod(tdelta.seconds, 3600)
    d['min'], d['sec'] = divmod(rem, 60)

    if d['min'] == 0:
        fmt = '{sec} sec'
    elif d['hrs'] == 0:
        fmt = '{min} min {sec} sec'
    elif d['days'] == 0:
        fmt = '{hrs} hr(s) {min} min {sec} sec'
    else:
        fmt = '{days} day(s) {hrs} hr(s) {min} min {sec} sec'

    return fmt.format(**d)


def send_email(job_id, query_str=None, to='gianluca.rigoletti@cern.ch'):
    import smtplib, ssl
    from email.message import EmailMessage
    from email.mime.text import MIMEText
    ssl_context = ssl.create_default_context()
    # don't check if certificate hostname doesn't match target hostname
    ssl_context.check_hostname = False
    # don't check if the certificate is trusted by a certificate authority
    ssl_context.verify_mode = ssl.CERT_NONE
    SENDER_PASSWORD = 'ZuzuNewe6'
    SENDER_EMAIL = 'gaselog@cern.ch'
    BASE_URL = 'https://rpc-monitoring-status.web.cern.ch'
    content = f'''
    Hello, your query is ready. 
    The query was <pre>{query_str}<pre>
    You can check of your job id 
    <a href="{BASE_URL}/{job_id}">{BASE_URL}/{job_id}</a>

    Cheers,
    gaselog
    '''
    msg = MIMEText(content, 'html')
    msg['Subject'] = f'Your query is ready'
    msg['From'] = SENDER_EMAIL
    msg['To'] = to
    # Send the message via our own SMTP server.
    s = smtplib.SMTP('smtp.cern.ch')
    s.starttls(context=ssl_context)
    s.login(SENDER_EMAIL, SENDER_PASSWORD)
    s.send_message(msg)
    s.quit()


def save_data(data, job_id, savepath='/data', method='aligned'):
    df = pd.DataFrame()
    savepath = Path(savepath)
    for key, (times, values) in data.items():
        dftemp = pd.DataFrame(values, index=times, columns=[key])
        df = df.join(dftemp, how='outer')
    if method == 'aligned':
        filepath = savepath / f'{job_id}-{method}.csv'
        df.to_csv(filepath)
    elif method == 'aligned-filled-rounded':
        filepath = savepath / f'{job_id}-{method}.csv'
        df.fillna(method='ffill').round(decimals=1).to_csv(filepath)
    elif method == 'aligned-all':
        filepath = savepath / f'{job_id}-{method}.csv'
        df.fillna(method='ffill').to_csv(filepath)             

    return filepath