import requests
import time
import pytimber
import pandas as pd
import numpy as np
from rq import get_current_job
from utilities import send_email, save_data
import os
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

SAVE_PATH = os.environ.get('SAVE_PATH', '/data')

def search_variables(query):
    ldb = pytimber.LoggingDB()
    res = ldb.search(query)
    return res


def get_values(query, t1, t2):
    """Emulated pytimber.LoggingDB.get() with a small difference.
    It retrieves the last stored point before t1 and set it to t1. 
    It is useful to understand what was the value before the time of t1.
    :param t1: str, pd.Timestamp, datetime.datetime 
    :param t2: str, pd.Timestamp, datetime.datetime 
    :return: a dictionary similar to the one of LoggingDB.get()
    :rtype: dict
    """
    import time as t
    start = t.time()
    ldb = pytimber.LoggingDB()
    t1, t2 = pd.to_datetime(t1), pd.to_datetime(t2)
    initial_points = ldb.get(query, t1, None, unixtime=False)
    middle_points = ldb.get(query, t1, t2, unixtime=False)
    res = {}
    points = 0
    for key, (time, value) in middle_points.items():
        times = np.array([t1.to_pydatetime()] + time.tolist())
        values = np.array(initial_points[key][1].tolist() + value.tolist())
        times = np.append(times, t2.to_pydatetime())
        values = np.append(values, values[-1])
        res[key] = (times, values)
        points += len(times)
    end = t.time()
    job = get_current_job()
    job.meta['duration'] = (end - start)
    job.meta['points'] = points
    job.save_meta()
    return res

def count_words_at_url(url):
    resp = requests.get(url)
    if resp.status_code != 200:
        return f'Error: status code: {resp.status_code}'
    length = len(resp.text.split())
    print('length: ', length, flush=True)
    return length

def start_job(query, t1, t2, email_to=None):
    result = get_values(query, t1, t2)
    job = get_current_job()
    job_id = job.get_id()
    filepath = save_data(result, job_id, savepath=SAVE_PATH)
    job.meta['filepath'] = filepath
    logger.info(f'Saving data of id {job_id} to {SAVE_PATH}')
    if email_to:
        query_str = job.meta.get('query_str')
        send_email(job_id, query_str, email_to)
    return result

main_function = start_job
