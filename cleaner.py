import os
from redis import Redis
from pathlib import Path

REDIS_HOST = os.environ.get('REDIS_SERVICE_HOST', 'redis')
REDIS_PORT = os.environ.get('REDIS_SERVICE_PORT', '6379')
FOLDER_PATH = Path(os.environ.get('FOLDER_PATH', '/data'))
redis_conn = Redis(host=REDIS_HOST, port=int(REDIS_PORT))
pubsub = redis_conn.pubsub()

pubsub.psubscribe('__keyspace@*')
for msg in pubsub.listen():
    if msg.get('data') == b'expired':
        channel = msg.get('channel', None)
        if not channel: continue
        channel = channel.decode('utf8')
        if 'rq:job' in channel:
            key = channel.split(':')[-1]
            print('Received notification. Deleting ', key)
            file_list = FOLDER_PATH.glob(f'{key}*')
            for file in file_list:
                print('deleting ', file)
                file.unlink()
