FROM ubuntu:latest
WORKDIR /code
RUN apt-get update && apt-get install -y python3.8
ENV TZ=Europe/Rome
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ENV DEBIAN_FRONTEND=noninteractive 
RUN apt-get install -y python3-pip 
RUN apt-get install -y openjdk-11-jdk
RUN apt-get install -y rsync
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV USER=webtimber

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
RUN python3 -c "import pytimber; ldb=pytimber.LoggingDB()"

RUN chown -R 1001:1001 /code
RUN mkdir /data
RUN chown -R 1001:1001 /data
USER 1001

CMD ["python3", "app.py"]